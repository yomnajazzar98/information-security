package Cryptography;// Java program to generate
// a symmetric key


import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;

public class symmetric {

    static GCMParameterSpec spec;
    private static final String key = "aesEncryptionKey";
    private static final int RANDOM_BYTES_LENGTH = 16;
    private static final SecureRandom secureRandom = new SecureRandom();
    private static final String HMAC_SHA512 = "HmacSHA512";
    private static final String STRING_ENCODING = "ISO_8859_1";
    // Function to create a secret key
    public static SecretKey createAESKey(String mobile)
            throws Exception {
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        KeySpec passwordBasedEncryptionKeySpec = new PBEKeySpec(mobile.toCharArray(), key.getBytes(), 12288, 256);
        SecretKey secretKeyFromPBKDF2 = secretKeyFactory.generateSecret(passwordBasedEncryptionKeySpec);
        return  new SecretKeySpec(secretKeyFromPBKDF2.getEncoded(), "AES");
    }

    public static SecretKey  GenerateSessionKey() throws NoSuchAlgorithmException {
        KeyGenerator keygenerator = KeyGenerator.getInstance("AES");
       return   keygenerator.generateKey();
    }


    public static GCMParameterSpec getGCMParameterSpec(byte[] nonce) {
        // final byte[] nonce = new byte[32];
        //random.nextBytes(nonce);

        GCMParameterSpec spec = new GCMParameterSpec(16 * 8, nonce);
        return spec;

    }

    public static byte[] MAC(ArrayList request,SecretKey SymmetricKey) throws Exception {

        Mac mac = Mac.getInstance("HMACSHA1");
        mac.init(SymmetricKey);
        mac.update(request.toString().getBytes());
        byte[] macResult = mac.doFinal();

        //   System.out.println(java.util.Arrays.toString(mac.doFinal()));
        return macResult;
    }

//    cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
//    SecretKeySpec secretKeySpec = new SecretKeySpec(KEY.getBytes(),"AES");
//    byte[] plainTextByte = plainText.getBytes();
//    IvParameterSpec ivParameterSpec = new IvParameterSpec(IV.getBytes());
//        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec , ivParameterSpec);
//    byte[] encryptedByte = cipher.doFinal(plainTextByte);
//    Base64.Encoder encoder = Base64.getEncoder();
//    String encryptedText = encoder.encodeToString(encryptedByte);
//        return encryptedText;
//

    public static String encrypt(String data, GCMParameterSpec GCM , SecretKey SymmetricKey) {
         String s = "";
        Cipher cipher = null;
        try {

            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, SymmetricKey);

            byte[] encrypted = cipher.doFinal(data.getBytes());
            s = Base64.getEncoder().encodeToString(encrypted);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }  catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
      return s;
    }
    private byte[] concatByteArrays(byte[] array1, byte[] array2) {
        byte[] result = new byte[array1.length + array2.length];
        System.arraycopy(array1, 0, result, 0, array1.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        return result;
    }
    byte[] getMac( SecretKey SymmetricKey, String value) throws InvalidKeyException, NoSuchAlgorithmException
    {
        Mac sha512Hmac;
        final byte[] byteKey = key.getBytes(StandardCharsets.UTF_8);
        sha512Hmac = Mac.getInstance(HMAC_SHA512);
        int macLength = Mac.getInstance(HMAC_SHA512).getMacLength();
       // System.out.println(macLength);
        SecretKeySpec keySpec = new SecretKeySpec(byteKey, HMAC_SHA512);
        sha512Hmac.init(keySpec);
        byte[] macData = sha512Hmac.doFinal(value.getBytes(StandardCharsets.UTF_8));

        // Can either base64 encode or put it right into hex
        //result = Base64.getEncoder().encodeToString(macData);
        return macData;
    }

    public String encryptt(String value,SecretKey SymmetricKey) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeyException, InvalidAlgorithmParameterException, BadPaddingException
    {
        byte[] myMac;
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        byte[] paddingBytes = new byte[RANDOM_BYTES_LENGTH];
        secureRandom.nextBytes(paddingBytes);
        IvParameterSpec iv =  new IvParameterSpec(paddingBytes);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
        byte[] encryptedText = cipher.doFinal(value.getBytes());
        // Prepend random to the encryptedText
        byte[] paddedCipher = concatByteArrays(paddingBytes, encryptedText);
        myMac=getMac(SymmetricKey,value);
      //  System.out.println("my mac lenth"+myMac.length);
        byte[] all=concatByteArrays(myMac, paddedCipher);
        BASE64Encoder base64Encoder = new BASE64Encoder();
        System.out.println("the statement encrypted");
        return base64Encoder.encode(all);
    }
    public String decryptt(String encrypted, SecretKey SymmetricKey) throws NoSuchAlgorithmException, IOException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
    {
        BASE64Decoder base64decoder = new BASE64Decoder();
        int macLength = Mac.getInstance(HMAC_SHA512).getMacLength();
        byte[] completeBytes = base64decoder.decodeBuffer(encrypted);
        int paddStartIndex = completeBytes.length - macLength;
       // System.out.println("mahaaaaa"+encrypted.length());
        byte[] mac = Arrays.copyOfRange(completeBytes, 0, macLength);
        byte[] padding = Arrays.copyOfRange(completeBytes, macLength, RANDOM_BYTES_LENGTH+macLength);
        byte[] text = Arrays.copyOfRange(completeBytes,RANDOM_BYTES_LENGTH+macLength,completeBytes.length);
        IvParameterSpec iv = new IvParameterSpec(padding);
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
        String plaintext = new String(cipher.doFinal(text), STRING_ENCODING);
        byte[] mymac=getMac(SymmetricKey,plaintext);
        if(Arrays.equals(mymac, mac)) {
            return plaintext;
        }
        else return "Message corrupted";
    }

    public static String decrypt(String data, GCMParameterSpec GCM, SecretKey SymmetricKey) {
        byte[] encrypted = Base64.getDecoder().decode(data);

        String s = "";
        Cipher cipher = null;
        try {

            cipher = Cipher.getInstance("AES");

            cipher.init(Cipher.DECRYPT_MODE, SymmetricKey);

            byte[] decrypted = cipher.doFinal(encrypted);
             s = new String(decrypted, StandardCharsets.UTF_8);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }  catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }


        return s;
    }



}
