import Cryptography.Hyper;
import Cryptography.symmetric;
import DigitalSignature.DigitalSignature;
import SignedCertigicate.*;
import SignedCertigicate.Certificate;
import com.google.gson.Gson;

import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;


public class ClientSide {
    static symmetric symmetric = new symmetric();
    SecretKey symmetrickey = null;
    byte[] nonce = new byte[32];
    GCMParameterSpec GCM =null;
    SecureRandom random = SecureRandom.getInstanceStrong();
    static KeyPair keypair;
    Socket socket = null;
    ObjectOutputStream oos=null;
    ObjectInputStream ois=null;
    PublicKey ServerPublickey=null;
    PublicKey CAPublickey =null;
    public String mobile="#";
    static Certificate certificate;
    PublicKey mypublickey;
    PrivateKey myprivatekey;
    ArrayList<String> User_request = new ArrayList<String>();
    Scanner sc_Int = new Scanner(System.in);
    Scanner sc_Str = new Scanner(System.in);



    public static void generatorKeyPair() throws Exception {
        keypair =  Hyper.generateKeyPair();
        System.out.println(
                "The Public Key is: "
                        + DatatypeConverter.printHexBinary(
                        keypair.getPublic().getEncoded()));
        System.out.println(
                "The Private Key is: "
                        + DatatypeConverter.printHexBinary(
                        keypair.getPrivate().getEncoded()));
    }
    public  void get_privateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        String pub,pr;

        BufferedReader brTest = new BufferedReader(new FileReader(mobile+".txt"));
        KeyFactory   keyFactory = KeyFactory.getInstance("RSA");

        pub = brTest .readLine();
        byte[]  buffer = DatatypeConverter.parseHexBinary(pub);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
        mypublickey = keyFactory.generatePublic(keySpec);
        System.out.println("Public is : " + DatatypeConverter.printHexBinary(mypublickey.getEncoded()));

        pr = brTest .readLine();
        byte[] clear = pr.getBytes();
        PKCS8EncodedKeySpec keySpec2 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(pr));
        myprivatekey=keyFactory.generatePrivate(keySpec2);
        System.out.println("Private is : " + DatatypeConverter.printHexBinary(myprivatekey.getEncoded()));

    }
    public void SavePrivatKeyInFile()
    {
        try {
            FileWriter myWriter = new FileWriter(mobile + ".txt");
            myWriter.write(DatatypeConverter.printHexBinary(
                    keypair.getPublic().getEncoded())+"\n");
            myWriter.write(Base64.getEncoder().encodeToString(
                    keypair.getPrivate().getEncoded()));
            myWriter.close();
            System.out.println("Successfully wrote to the file " + mobile);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
    public ClientSide() throws Exception {
        InetAddress host = InetAddress.getLocalHost();
        socket = new Socket(host.getHostName(), 9877);
        oos=new ObjectOutputStream(socket.getOutputStream());
        ois=new ObjectInputStream(socket.getInputStream());
        random.nextBytes(nonce);
        GCM =this.symmetric.getGCMParameterSpec(nonce);


        Boolean bool1 = true;
        while (bool1) {

            if (mobile.equals("#")) {

                System.out.println("Enter\n1:SignUp \n2:Login \n3:exit");
                int Type = sc_Int.nextInt();
                switch (Type) {
                    case 1: {
                        //for 5 level
                        generatorKeyPair();
                        User_request = new ArrayList<String>();
                        User_request.add("signup");
                        System.out.println("enter mobile");
                        User_request.add(sc_Str.nextLine());
                        System.out.println("enter password");
                        User_request.add(sc_Str.nextLine());
                        //for 5 level
                        User_request.add(DatatypeConverter.printHexBinary(keypair.getPublic().getEncoded()));
                        oos.writeObject(User_request); // writing to the underlying output stream
                        String responce = (String) ois.readObject();

                        if (responce.equals("true")) {
                            //******************************************************************
                            this.mobile = User_request.get(1);
                            //******************************************************************

                            //for 5 level
                            SavePrivatKeyInFile();
                            get_privateKey();
                        } else {
                            System.out.println(responce);
                        }
                        User_request.clear();

                        break;
                    }

                    case 2: {
                        User_request = new ArrayList<String>();
                        User_request.add("login");
                        System.out.println("enter mobile");
                        User_request.add(sc_Str.nextLine());
                        System.out.println("enter password");
                        User_request.add(sc_Str.nextLine());
                        oos.writeObject(User_request); // writing to the underlying output stream
                        String responce = (String) ois.readObject();

                        if (responce.equals("true")) {
                            //******************************************************************
                            this.mobile = User_request.get(1);
                            //******************************************************************
                            get_privateKey();

                        } else {
                            System.out.println(responce);
                        }
                        User_request.clear();
                        break;
                    }
                    case 3: {
                        bool1=false;
                        break;
                    }
                }
            }
            else{
                System.out.println("Enter\n0:no Encryption \n1:symmetric Encryption \n2:Hyper Encryption \n3:Asymmetric+digital Encryption \n4:end to end Encryption\n5:exit");
            int EncryptType = sc_Int.nextInt();
            switch (EncryptType) {
                case 0: {//none
                    Boolean bool2 = true;
                    while (bool2) {
                        System.out.println("\nEnter\n1:sent a message \n2:sent a message to new number \n3:show message  \n4:exit");
                        int choose = sc_Int.nextInt();
                        User_request.clear();
                        switch (choose) {

                            case 1: {
                                User_request = new ArrayList<String>();
                                sent_message("sent_message");
                                oos.writeObject(User_request); // writing to the underlying output stream
                                String responce = (String) ois.readObject();
                                System.out.println(responce);
                                User_request.clear();
                                break;
                            }
                            case 2: {
                                User_request = new ArrayList<String>();
                                sent_message_Newnum("sent_message_to_new_num");
                                oos.writeObject(User_request); // writing to the underlying output stream
                                String responce = (String) ois.readObject();
                                System.out.println(responce);
                                User_request.clear();
                                break;
                            }
                            case 3: {
                                User_request = new ArrayList<String>();
                                show_message("show_message");
                                oos.writeObject(User_request); // writing to the underlying output stream
                                String res = (String) ois.readObject();
                                System.out.print("User messages: ");
                                System.out.println(res);
                                User_request.clear();
                                break;
                            }
                            case 4: {
                                bool2 = false;
                                break;
                            }
                        }

                    }
                    break;


                }
                case 1: {
                    Boolean bool2 = true;
                    while (bool2) {
                        System.out.println("symmetric Encryption");
                        System.out.println("Enter\n1:sent a message \n2:sent a message to new number \n3:show message \n4:exit");
                        int choose = sc_Int.nextInt();
                        switch (choose) {
                            case 1: {
                                User_request = new ArrayList<String>();
                                sent_message("sent_message_encrypted");
                                EncryptMessageAndSentData(this.symmetric.createAESKey(User_request.get(1)));
                                break;
                            }
                            case 2: {
                                User_request = new ArrayList<String>();
                                sent_message_Newnum("sent_message_encrypted_new_num");
                                EncryptMessageAndSentData(this.symmetric.createAESKey(User_request.get(1)));
                                break;
                            }
                            case 3: {
                                User_request = new ArrayList<String>();
                                show_message("show_encrypt_message");
                                oos.writeObject(User_request);
                                ArrayList<String> responce = (ArrayList) ois.readObject();
                                ArrayList<String> decrypted = new ArrayList<String>();
                                String receivedMAc = responce.get(responce.size() - 1);
                                responce.remove(responce.size() - 1);
                                String Mac = java.util.Arrays.toString(symmetric.MAC(responce, this.symmetric.createAESKey(User_request.get(1))));
                                System.out.println("Mac :" + Mac);
                                if (!Mac.equals(receivedMAc)) {
                                    System.err.println("The data is incorrect, it has been modified");

                                } else {
                                    System.out.println("matched MAC");
                                    for (int i = 0; i < responce.size(); i++) {
                                        System.out.println(this.symmetric.decryptt(responce.get(i), this.symmetric.createAESKey(User_request.get(1))));
                                    }
                                }
                                User_request.clear();
                                break;
                            }
                            case 4: {
                                bool2 = false;
                                break;
                            }
                        }

                    }
                    break;
                }
                case 2: {
                    User_request = new ArrayList<String>();
                    User_request.add("get_publickey");
                    oos.writeObject(User_request);

                    //receive server public key
                    ServerPublickey = (PublicKey) ois.readObject();

                    System.out.println("ServerPublickey Key is: " + DatatypeConverter.printHexBinary(ServerPublickey.getEncoded()));

                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

                    //Generate session Key
                    symmetrickey = symmetric.GenerateSessionKey();
                    System.out.println("The Session Key is :" + DatatypeConverter.printHexBinary(symmetrickey.getEncoded()));
                    //Encrypt session key
                    byte[] EncryptedKey = Hyper.Encrept(DatatypeConverter.printHexBinary(symmetrickey.getEncoded()), ServerPublickey);

                    //send session key
                    System.out.println("The Encrypted Session Key is :" + DatatypeConverter.printHexBinary(EncryptedKey));
                    out.println(DatatypeConverter.printHexBinary(EncryptedKey));

                    //receive acceptance message
                    String responcee = (String) ois.readObject();
                    System.out.println(responcee);
                    User_request.clear();
                    Boolean bool2 = true;
                    while (bool2) {
                        System.out.println("Hyper Encryption");
                        System.out.println("Enter\n1:sent message \n2:sent a message to new number \n3:show message\n4:exit");
                        int choose = sc_Int.nextInt();
                        switch (choose) {
                            case 1: {
                                User_request = new ArrayList<String>();
                                sent_message("sent_message_encrypted_Asymmetric");
                                EncryptMessageAndSentData(symmetrickey);
                                break;
                            }
                            case 2: {
                                User_request = new ArrayList<String>();
                                sent_message_Newnum("sent_message_encrypted_Asymmetric_new_num");
                                EncryptMessageAndSentData(symmetrickey);
                                break;
                            }
                            case 3: {
                                User_request = new ArrayList<String>();
                                show_message("show_encrypt_message_Asymmetric");
                                oos.writeObject(User_request);
                                ArrayList<String> responce = (ArrayList) ois.readObject();
                                ArrayList<String> decrypted = new ArrayList<String>();
                                String receivedMAc = responce.get(responce.size() - 1);
                                responce.remove(responce.size() - 1);
                                String Mac = java.util.Arrays.toString(symmetric.MAC(responce,symmetrickey));
                                System.out.println("Mac :" + Mac);
                                if (!Mac.equals(receivedMAc)) {
                                    System.err.println("The data is incorrect, it has been modified");

                                } else {
                                    System.out.println("matched MAC");
                                    for (int i = 0; i < responce.size(); i++) {
                                        System.out.println(this.symmetric.decryptt(responce.get(i), symmetrickey));
                                    }
                                }
                                User_request.clear();
                                break;

                            }
                            case 4: {
                                bool2 = false;
                                break;
                            }
                        }

                    }

                    break;
                }
                case 3: {
                    generatorKeyPair();
                    User_request = new ArrayList<String>();
                    User_request.add("get_key");
                    oos.writeObject(User_request);
                    //send public key
                    PublicKey publicKey = keypair.getPublic();
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    out.println(DatatypeConverter.printHexBinary(publicKey.getEncoded()));
                    //receive server public key
                    Scanner in = new Scanner(socket.getInputStream());
                    String strSPuK = in.nextLine();
                    byte[] buffer = DatatypeConverter.parseHexBinary(strSPuK);
                    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
                    PublicKey serverPuK = keyFactory.generatePublic(keySpec);
                    System.out.println("Server Public key : " + DatatypeConverter.printHexBinary(serverPuK.getEncoded()));

                    Boolean bool3 = true;
                    while (bool3) {
                        System.out.println("digital Encryption");
                        System.out.println("Enter\n1:sent message \n2:sent message to new number \n3:show message\n4:exit");
                        int choose = sc_Int.nextInt();
                        switch (choose) {
                            case 1: {
                                User_request = new ArrayList<String>();
                                sent_message("sent_message_encrypted_digital");
//                               //sign input
                                String signRequest = getSignMessage(User_request.get(User_request.size() - 1), serverPuK);
                                User_request.set(User_request.size() - 1, signRequest);
                                //send signed data to the socket
                                oos.writeObject(User_request);

                                //read the socket response
                                String signResponse = in.nextLine();
                                System.out.println("receive signed response: " + signResponse);
                                //get [response,signing]
                                String str[] = getResponseSeparateSign(signResponse);
                                //decrypt response
                                String response = Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]), keypair.getPrivate());
                                //verify sign
                                if (!DigitalSignature.Verify_Digital_Signature(
                                        response.getBytes(),
                                        DatatypeConverter.parseHexBinary(str[1]), serverPuK))
                                    System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
                                else {
                                    System.out.println(response);
                                }

                                break;
                            }
                            case 2: {
                                User_request = new ArrayList<String>();
                                sent_message_Newnum("sent_message_encrypted_digital_new_num");
//                               //sign input
                                String signRequest = getSignMessage(User_request.get(User_request.size() - 1), serverPuK);
                                User_request.set(User_request.size() - 1, signRequest);
                                //send signed data to the socket
                                oos.writeObject(User_request);
                                //read the socket response
                                String signResponse = in.nextLine();
                                System.out.println("receive signed response: " + signResponse);
                                //get [response,signing]
                                String str[] = getResponseSeparateSign(signResponse);
                                //decrypt response
                                String response = Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]), keypair.getPrivate());
                                //verify sign
                                if (!DigitalSignature.Verify_Digital_Signature(
                                        response.getBytes(),
                                        DatatypeConverter.parseHexBinary(str[1]), serverPuK))
                                    System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
                                else {
                                    System.out.println(response);
                                }

                                break;
                            }
                            case 3: {
                                User_request = new ArrayList<String>();
                                show_message("show_message_encrypted_digital");
                                System.out.println(User_request);

//                               //sign input
                                // String signRequest=getSignMessage(User_request.toString(),serverPuK);
                                String signRequest = getSignMessage(User_request.get(User_request.size() - 1), serverPuK);
                                User_request.set(User_request.size() - 1, signRequest);
                                oos.writeObject(User_request);
//                               //send signed data to the socket
                                System.out.println("sending signed data ..");
//
//                             //read the socket response
                                String signResponse = in.nextLine();
                                System.out.println("receive signed response: " + signResponse);
                                //get [response,signing]
                                String str[] = getResponseSeparateSign(signResponse);
                                //decrypt response
                                String response = Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]), keypair.getPrivate());
//                             System.out.println("decrypted message "+response);
                                //verify sign
                                if (!DigitalSignature.Verify_Digital_Signature(
                                        response.getBytes(),
                                        DatatypeConverter.parseHexBinary(str[1]), serverPuK))
                                    System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
                                else {
                                    System.out.println(response);
                                }

                                break;
                            }
                            case 4: {
                                bool3 = false;
                                break;
                            }
                        }

                    }

                    break;
                }

                case 4: {
                    System.out.println("certificate is  " + certificate);
                    if (certificate == null)
                        getCertificate();

//                    if (CAPublickey == null)
                        getCApublickey();
                    System.out.println("CAPublickey " + DatatypeConverter.printHexBinary(CAPublickey.getEncoded()));
                    User_request = new ArrayList<String>();
                    User_request.add("exchange_the_certificate");
                    oos.writeObject(User_request);

                    //read Server Certificate
                        Certificate ServerCertificate = (Certificate) ois.readObject();

                    //sent Certificate to Server
                    oos.writeObject(certificate);

                    if (!DigitalSignature.Verify_Digital_Signature(ServerCertificate.Subject.getBytes(), ServerCertificate.Signature, CAPublickey)) {
                        System.out.println("The signature is incorrect ... Retry sending");
                        break;
                    }
                    if (!ServerCertificate.Subject.equals("Url : Server1.com \n Phone:0992039461")) {
                        System.out.println("The address is incorrect ... Retry sending");
                        break;
                    }
                    System.out.println("Server Certificate is " + ServerCertificate.Subject + " " + ServerCertificate.Signature.length + "\n" + DatatypeConverter.printHexBinary(ServerCertificate.publicKey.getEncoded()));
                    ServerPublickey = ServerCertificate.publicKey;
                    Scanner in = new Scanner(socket.getInputStream());

                    Boolean bool2 = true;
                    while (bool2) {
                        System.out.println("End To End Eccrypt");
                        System.out.println("Enter\n1:sent a message \n2:sent a message to new number \n3:show message\n4:exit");
                        int choose = sc_Int.nextInt();
                        switch (choose) {
                            case 1: {
                                User_request = new ArrayList<String>();
                                sent_message_EndToEnd("sent_message_endToend");
                                System.out.println(User_request);
                                oos.writeObject(User_request);
                                String signRequest = getSignMessage1(User_request.get(2), ServerPublickey,myprivatekey);
                                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                                out.println(signRequest);

                                //read the socket response
                                String signResponse = in.nextLine();
                                System.out.println("receive signed response: " + signResponse);
                                //get [response,signing]
                                String str[] = getResponseSeparateSign(signResponse);
                                //decrypt response
                                String response = Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]), myprivatekey);
//                             System.out.println("decrypted message "+response);
                                //verify sign
                                if (!DigitalSignature.Verify_Digital_Signature(
                                        response.getBytes(),
                                        DatatypeConverter.parseHexBinary(str[1]), ServerPublickey))
                                    System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
                                else {
                                    System.out.println(response);
                                }
                               // System.out.println((String)ois.readObject());
                                break;
                            }
                            case 2: {
                                User_request = new ArrayList<String>();
                                sent_message_EndToEnd_Newnum("sent_message_endToend_new_num");
                                System.out.println(User_request);
                                oos.writeObject(User_request);
                                String signRequest = getSignMessage1(User_request.get(2), ServerPublickey,myprivatekey);
                                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                                out.println(signRequest);

                                //read the socket response
                                String signResponse = in.nextLine();
                                System.out.println("receive signed response: " + signResponse);
                                //get [response,signing]
                                String str[] = getResponseSeparateSign(signResponse);
                                //decrypt response
                                String response = Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]), myprivatekey);
//                             System.out.println("decrypted message "+response);
                                //verify sign
                                if (!DigitalSignature.Verify_Digital_Signature(
                                        response.getBytes(),
                                        DatatypeConverter.parseHexBinary(str[1]), ServerPublickey))
                                    System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
                                else {
                                    System.out.println(response);
                                }
                               // System.out.println((String)ois.readObject());
                                break;
                            }
                            case 3: {
                                User_request = new ArrayList<String>();
                                show_message("show_message_EndToEnd");
                                oos.writeObject(User_request);
                                String signRequest = getSignMessage1(User_request.get(1), ServerPublickey,myprivatekey);
                                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                                out.println(signRequest);
                                ArrayList<String> responce = (ArrayList<String>) ois.readObject();
                                System.out.println(responce);
                                for (int i=0 ; i<responce.size();i++)
                                System.out.println("message "+i+"from " +Hyper.DecrepR(DatatypeConverter.parseHexBinary(responce.get(i)),myprivatekey));
                                User_request.clear();
                                break;
                            }
                            case 4: {
                                bool2 = false;
                                break;
                            }
                        }

                    }

                    break;
                }

                case 5: {
                    bool1 = false;
                    break;
                }

            }
        }
        }

        //close resources
        oos.close();
        ois.close();
    }

    private void getCertificate() throws Exception {
        System.out.println("getCertificate");
        try (Socket socket = new Socket("127.0.0.1", 22222)) {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream  ois = new ObjectInputStream(socket.getInputStream());

            //sent CSR to CA
            ArrayList ClientRequest = new ArrayList();
            System.out.println(mobile+DatatypeConverter.printHexBinary(mypublickey.getEncoded()));
            CSR csr = new CSR(mobile, mypublickey);
            ClientRequest.add(CAMultiThreading.CREATE);
            ClientRequest.add(csr);
            oos.writeObject(ClientRequest);

            //get Certificate from CA
            certificate = (Certificate) ois.readObject();

            System.out.println("Client Certificate is "+ certificate.Subject +" "+certificate.Signature.length);

        }

    }

    private void getCApublickey() throws Exception {
        try (Socket socket = new Socket("127.0.0.1", 22222)) {
            ObjectOutputStream   oos = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            ArrayList ClientRequest = new ArrayList();
            ClientRequest.add(CAMultiThreading.PubKey);
            oos.writeObject(ClientRequest);
            //get CA publickey from CA
            CAPublickey= (PublicKey) ois.readObject();
        }
    }
    public void sent_message(String title)
    {
        ConnectToDatabase connect=new ConnectToDatabase();
        User_request.add(title);
//        System.out.println("enter your mobile phone");
//        User_request.add(sc_Str.nextLine());
        User_request.add(mobile);
        String res= connect.getAllNumberContact(User_request.get(1));
        System.out.println("Choose Number:");
        System.out.println(res);
        System.out.println("enter mobile phone client");
        User_request.add(sc_Str.nextLine());

        System.out.println("enter message for client");
        User_request.add(sc_Str.nextLine());

    };
    public void sent_message_Newnum(String title)
    {
        ConnectToDatabase connect=new ConnectToDatabase();
        User_request.add(title);
//        System.out.println("enter your mobile phone");
//        User_request.add(sc_Str.nextLine());
        User_request.add(mobile);
        String res= connect.getAllNumber(User_request.get(1));
        System.out.println("Choose Number To Add To Contacts And Send Message:");
        System.out.println(res);
        System.out.println("enter mobile phone client");
        User_request.add(sc_Str.nextLine());
        String res1= connect.Contacts(User_request.get(1),User_request.get(2));
        System.out.println(res1);
        System.out.println("enter message for client");
        User_request.add(sc_Str.nextLine());
    };
    public void sent_message_EndToEnd(String title) throws IOException {
        try {
            ConnectToDatabase connect=new ConnectToDatabase();
            User_request.add(title);
            User_request.add(mobile);
            String res= connect.getAllNumberContact(User_request.get(1));
            System.out.println("Choose Number:");
            System.out.println(res);
            System.out.println("enter mobile phone client");
            User_request.add(sc_Str.nextLine());
            //get client public key
            ArrayList<String> User_request2=new ArrayList<>();
            User_request2.add("get_PubClient");
            User_request2.add(User_request.get(User_request.size()-1));
            oos.writeObject(User_request2);
            String pubclient = (String)ois.readObject();
            System.out.println(pubclient);
            byte[] buffer = DatatypeConverter.parseHexBinary(pubclient);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            PublicKey clientPuK = keyFactory.generatePublic(keySpec);
            System.out.println("enter message for client");
            String s=mobile+":"+sc_Str.nextLine();
//            byte[] digitSign= DigitalSignature.Create_Digital_Signature(s.getBytes(),myprivatekey);
//            String signResponse=DatatypeConverter.printHexBinary(digitSign);
            User_request.add(DatatypeConverter.printHexBinary(Hyper.Encrept(s,clientPuK)));
          // User_request.add(signResponse);
            ///  System.out.println("message "+Hyper.DecrepR(DatatypeConverter.parseHexBinary(User_request.get(User_request.size()-1)),myprivatekey));

        } catch (ClassNotFoundException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    };
    public void sent_message_EndToEnd_Newnum(String title) throws IOException {
        try {
            ConnectToDatabase connect=new ConnectToDatabase();
            User_request.add(title);
            User_request.add(mobile);
            String res= connect.getAllNumber(User_request.get(1));
            System.out.println("Choose Number To Add To Contacts And Send Message:");
            System.out.println(res);
            System.out.println("enter mobile phone client");
            User_request.add(sc_Str.nextLine());
            String res1= connect.Contacts(User_request.get(1),User_request.get(2));
            System.out.println(res1);
            //get client public key
            ArrayList<String> User_request2=new ArrayList<>();
            User_request2.add("get_PubClient");
            User_request2.add(User_request.get(User_request.size()-1));
            oos.writeObject(User_request2);
            String pubclient = (String)ois.readObject();
            System.out.println(pubclient);
            byte[] buffer = DatatypeConverter.parseHexBinary(pubclient);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            PublicKey clientPuK = keyFactory.generatePublic(keySpec);
            System.out.println("enter message for client");
            String s=mobile+":"+sc_Str.nextLine();
            User_request.add(DatatypeConverter.printHexBinary(Hyper.Encrept(s,clientPuK)));
//            System.out.println("message "+Hyper.DecrepR(DatatypeConverter.parseHexBinary(User_request.get(User_request.size()-1)),myprivatekey));


        } catch (ClassNotFoundException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    };



    public void show_message(String title)
    {
        User_request.add(title);
//        System.out.println("enter your mobile phone");
//        User_request.add(sc_Str.nextLine());
        User_request.add(mobile);
    };


    public void EncryptMessageAndSentData(SecretKey secretKey) throws Exception {
        String message_encrypt=this.symmetric.encryptt(User_request.get(User_request.size()-1),secretKey);
        User_request.set(User_request.size()-1,message_encrypt);
        //sent Mac of request
        User_request.add(java.util.Arrays.toString(symmetric.MAC(User_request,secretKey)));
        oos.writeObject(User_request);
        System.out.print("responce: ");
        String responce = (String) ois.readObject();
        System.out.println(symmetric.decryptt(responce,secretKey));
        User_request.clear();

    }
    private static String getSignMessage(String message,PublicKey key)throws Exception{
        String result="";

        //generate signature
        byte[] digitSign= DigitalSignature.Create_Digital_Signature(message.getBytes(),keypair.getPrivate());

        //encrypt request
        byte[] cypherRequest= Hyper.Encrept(message,key);

        // cypherRequest + signature
        Map<String, String> map = new HashMap<>();
        map.put("request", DatatypeConverter.printHexBinary(cypherRequest));
        map.put("signature", DatatypeConverter.printHexBinary(digitSign));
        Gson gson = new Gson();
        result = gson.toJson(map);
        return result;
    }
    private static String getSignMessage1(String message,PublicKey key,PrivateKey p)throws Exception{
        String result="";

        //generate signature
        byte[] digitSign= DigitalSignature.Create_Digital_Signature(message.getBytes(),p);

        //encrypt request
        byte[] cypherRequest= Hyper.Encrept(message,key);

        // cypherRequest + signature
        Map<String, String> map = new HashMap<>();
        map.put("request", DatatypeConverter.printHexBinary(cypherRequest));
        map.put("signature", DatatypeConverter.printHexBinary(digitSign));

        Gson gson = new Gson();

        result = gson.toJson(map);

        return result;
    }
    private static String[] getResponseSeparateSign(String response) throws Exception {
        //parsing and extracting Request data
        Gson gson = new Gson();
        Map<String, String> req = gson.fromJson(response, Map.class);
        if(req.get("signature")!=null) {
            return new String[]{req.get("request"),req.get("signature")};
        } else {
            return null;
        }
    }
    private static String ReadResponse(String reponse){
        //parsing and extracting Request data
        Gson gson = new Gson();
        Map<String, String> req = gson.fromJson(reponse, Map.class);
        if(req.get("status").equals("true")) {
            return req.get("data");
        } else {
            return req.get("error");
        }
    }
    public static void main(String[] args) throws Exception {
        ClientSide c=new ClientSide();

    }

}