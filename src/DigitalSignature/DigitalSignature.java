package DigitalSignature;

import javax.xml.bind.DatatypeConverter;
import java.security.*;
import java.util.Scanner;

public class DigitalSignature {
    // Signing Algorithm
    private static final String
            SIGNING_ALGORITHM
            = "SHA256withRSA";
    private static final String RSA = "RSA";
    private static Scanner sc;

    // Function to implement Digital signature using SHA256 and RSA algorithm by passing private key.
    public static byte[] Create_Digital_Signature(byte[] input, PrivateKey Key) throws Exception
    {
        Signature signature
                = Signature.getInstance(
                SIGNING_ALGORITHM);
        signature.initSign(Key);
        signature.update(input);
        return signature.sign();
    }

    // Function for Verification of the digital signature by using the public key
    public static boolean Verify_Digital_Signature(byte[] input, byte[] signatureToVerify,
            PublicKey key) throws Exception
    {
        Signature signature
                = Signature.getInstance(
                SIGNING_ALGORITHM);
        signature.initVerify(key);
        signature.update(input);
        return signature
                .verify(signatureToVerify);
    }
    public static boolean Verify_Digital_Signature1(byte[] signatureToVerify,
                                                   PublicKey key) throws Exception
    {
        Signature signature
                = Signature.getInstance(
                SIGNING_ALGORITHM);
        signature.initVerify(key);
       // signature.update(input);
        return signature
                .verify(signatureToVerify);
    }
}
