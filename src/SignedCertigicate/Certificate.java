package SignedCertigicate;

import java.io.Serializable;
import java.security.PublicKey;

public class Certificate implements Serializable {

   public String Subject ;
   public PublicKey publicKey;
    public byte[] Signature;
    public  Certificate(String Subject , PublicKey publicKey , byte[] signature)
    {
        this.publicKey=publicKey;
        this.Subject=Subject;
        this.Signature=signature;
    }

}
