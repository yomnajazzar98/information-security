package SignedCertigicate;
import Cryptography.Hyper;
import DigitalSignature.DigitalSignature;
import com.google.gson.Gson;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.math.BigInteger;
import java.net.Socket;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

public class CertificateAuthority implements Runnable{
    private Socket socket;
    ObjectOutputStream oos=null;
    ObjectInputStream ois=null;
    ArrayList received=null;
    CertificateAuthority(Socket socket) throws IOException {
        this.socket=socket;
        oos=new ObjectOutputStream(socket.getOutputStream());
        ois=new ObjectInputStream(socket.getInputStream());
    }
    @Override
    public void run() {

        System.out.println("Connected: " + socket);

        try {
            System.out.println("new request");
            received=new ArrayList();
            received = (ArrayList) ois.readObject();
            String requestType = (String)received.get(0);

            System.out.println("request Type : "+requestType);



            if (requestType.equals(CAMultiThreading.CREATE)) {
                CSR csr = (CSR) received.get(1);

                System.out.println("Subject " + csr.Subject);
                createCertificateRequest(csr);
            }
            if (requestType.equals(CAMultiThreading.PubKey)) {
                getPublicckey();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void getPublicckey() throws Exception {
        oos.writeObject(CAMultiThreading.keyPair.getPublic());
    }
    private void createCertificateRequest(CSR csr) throws Exception {
        byte[] signature = DigitalSignature.Create_Digital_Signature(csr.Subject.getBytes(), CAMultiThreading.keyPair.getPrivate());
        Certificate certificate = new Certificate(csr.Subject, csr.publicKey, signature);
        oos.writeObject(certificate);
    }





}
