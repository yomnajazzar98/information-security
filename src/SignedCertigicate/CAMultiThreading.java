package SignedCertigicate;
//import Cryptography.Asymmetric;
import Cryptography.Hyper;
import sun.security.x509.CertificateX509Key;
//import textserver.TextServer;
import javax.xml.bind.DatatypeConverter;
import java.net.ServerSocket;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
public class CAMultiThreading {
    public static final String CREATE="create_certificate";
    public static final String PubKey="getCApublickey";
    public  static KeyPair keyPair;
    public static void main(String[] args) throws Exception {

        keyPair= Hyper.generateKeyPair();
        System.out.println(
                "The Public Key is: "
                        + DatatypeConverter.printHexBinary(
                        keyPair.getPublic().getEncoded()));
        System.out.println(
                "The Private Key is: "
                        + DatatypeConverter.printHexBinary(
                        keyPair.getPrivate().getEncoded()));

        try (ServerSocket listener = new ServerSocket(22222)) {
            System.out.print("The CA is running...");
            ExecutorService pool = Executors.newFixedThreadPool(20);
            while (true) {
                pool.execute(new CertificateAuthority(listener.accept()));
            }
        }
    }



}
