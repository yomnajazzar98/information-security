import Cryptography.Hyper;
import Cryptography.symmetric;
import DigitalSignature.DigitalSignature;
import SignedCertigicate.*;
import SignedCertigicate.Certificate;
import com.google.gson.Gson;

import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.lang.ClassNotFoundException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

/**
 * This class implements java Socket server
 * @author pankaj
 *
 */

public class ServerSide {
    static KeyPair keypair;
    static Certificate certificate;
    public static final String Subject="Url : Server1.com \n Phone:0992039461";
   static PublicKey CAPublickey=null;
    public static void main(String[] args)
    {
        ServerSocket server = null;
        try {
            // server is listening on port 1234
            server = new ServerSocket(9877);
            server.setReuseAddress(true);


            keypair =  Hyper.generateKeyPair();
            System.out.println(
                    "The Public Key is: "
                            + DatatypeConverter.printHexBinary(
                            keypair.getPublic().getEncoded()));
            System.out.println(
                    "The Private Key is: "
                            + DatatypeConverter.printHexBinary(
                            keypair.getPrivate().getEncoded()));

            // running infinite loop for getting
            // client request
            while (true) {

                // socket object to receive incoming client
                // requests
                Socket client = server.accept();

                // Displaying that new client is connected
                // to server
                System.out.println("New client connected"
                        + client.getInetAddress()
                        .getHostAddress());

                // create a new thread object
                ClientHandler clientSock
                        = new ClientHandler(client);

                // This thread will handle the client
                // separately
                new Thread(clientSock).start();
            }
        }
        catch (IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (server != null) {
                try {
                    server.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // ClientHandler class
    private static class ClientHandler implements Runnable {
        private final Socket clientSocket;
        ObjectOutputStream oos=null;
        ObjectInputStream ois = null;

        symmetric symmetric = new symmetric();
        SecretKey symmetrickey = null;
        SecureRandom random = SecureRandom.getInstanceStrong();
        PublicKey clientPK=null;
        byte[] nonce = new byte[32];
        GCMParameterSpec GCM =null;
    ConnectToDatabase connect=new ConnectToDatabase();;
       ArrayList<String> received=null;

        // Constructor
        public ClientHandler(Socket socket) throws IOException, NoSuchAlgorithmException {
            this.clientSocket = socket;
            //write object to Socket
             oos = new ObjectOutputStream(clientSocket.getOutputStream());
            //read from socket to ObjectInputStream object
             ois = new ObjectInputStream(clientSocket.getInputStream());
        }
        public void run()
        {
            Certificate ClientCertificate=null;
            random.nextBytes(nonce);
            GCM =this.symmetric.getGCMParameterSpec(nonce);
            try {
                while (true) {
                    System.out.println("new request");
                     received=new ArrayList<String>();
                     received = (ArrayList) ois.readObject();

                    for (int i = 0; i < received.size(); i++) {
                        System.out.println("received :" + received.get(i).toString());
                    }

                    if (received.get(0).equals("signup")) {
                        String password=received.get(2).toString();
                        String phone=received.get(1).toString();
                        String pub=received.get(3).toString();
                        String res= connect.Signup(phone,password,pub);
                        //   String res="hello";
                        System.out.println("res : "+res);
                        oos.writeObject(res);
                    }
                    else if (received.get(0).equals("login")) {
                        String password=received.get(2).toString();
                        String phone=received.get(1).toString();
                        String res= connect.Login(phone,password);
                        //   String res="hello";
                        System.out.println("res : "+res);
                        oos.writeObject(res);
                    }

                    else if (received.get(0).equals("sent_message")) {
                        String message=received.get(3).toString();
                        String res=storeMessage(message);
                        oos.writeObject(res);
                    }
                    else if (received.get(0).equals("sent_message_to_new_num")) {
                        String message=received.get(3).toString();
                        String res=storeMessage(message);
                        oos.writeObject(res);
                    }
                    else if (received.get(0).equals("show_message")) {
                        String res= getAllMessageForClient(received.get(1).toString());
                        oos.writeObject(res);
                    }
                    else if(received.get(0).equals("sent_message_encrypted"))
                    {
                        System.out.println("sent_message_encrypted");
                        //receive MAc from client
                        String receivedMAc=received.get(received.size() - 1);
                        received.remove(received.size()-1);
                        System.out.println("receivedMAc :"+receivedMAc);
                        String Mac= java.util.Arrays.toString(symmetric.MAC(received,this.symmetric.createAESKey(received.get(1))));
                        System.out.println("Mac :"+Mac);

                        if (!Mac.equals(receivedMAc)) {
                            System.err.println("WrongMac");
                            oos.writeObject(this.symmetric.encryptt("The data is incorrect, it has been modified",this.symmetric.createAESKey(received.get(1))));
                        }
                        else{
                            System.out.println("matched MAC");
                            //Decrypt the message by symmetric key of sending client
                            String message_decrypted=this.symmetric.decryptt(received.get(3),symmetric.createAESKey(received.get(1)));
                            System.out.println(message_decrypted);
                            String res=storeMessage(message_decrypted);
                            oos.writeObject(this.symmetric.encryptt(res ,this.symmetric.createAESKey(received.get(1))));


                        }
                    }
                    else if(received.get(0).equals("sent_message_encrypted_new_num"))
                    {
                        System.out.println("sent_message_encrypted");
                        //receive MAc from client
                        String receivedMAc=received.get(received.size() - 1);
                        received.remove(received.size()-1);
                        System.out.println("receivedMAc :"+receivedMAc);
                        String Mac= java.util.Arrays.toString(symmetric.MAC(received,this.symmetric.createAESKey(received.get(1))));
                        System.out.println("Mac :"+Mac);

                        if (!Mac.equals(receivedMAc)) {
                            System.err.println("WrongMac");
                            oos.writeObject(this.symmetric.encryptt("The data is incorrect, it has been modified",this.symmetric.createAESKey(received.get(1))));
                        }
                        else{
                            System.out.println("matched MAC");
                            //Decrypt the message by symmetric key of sending client
                            String message_decrypted=this.symmetric.decryptt(received.get(3),symmetric.createAESKey(received.get(1)));
                            System.out.println(message_decrypted);
                            String res=storeMessage(message_decrypted);
                            oos.writeObject(this.symmetric.encryptt(res,this.symmetric.createAESKey(received.get(1))));
                        }
                    }
                    else if (received.get(0).equals("show_encrypt_message")) {

                        String res= getAllMessageForClient(received.get(1).toString());
                        //phone = received.get(1).toString()
                        ArrayList<String> responce = new ArrayList<String>();
                        //encrypt of message
                        String   message_encrypted = this.symmetric.encryptt(res, this.symmetric.createAESKey(received.get(1)));
                        responce.add(message_encrypted);

                        //add mac
                        responce.add(java.util.Arrays.toString(symmetric.MAC(responce,this.symmetric.createAESKey(received.get(1)))));

                        //return  response
                        oos.writeObject(responce);
                    }
                    else if(received.get(0).equals("sent_message_encrypted_Asymmetric"))
                    {
                        System.out.println("sent_message_encrypted_Asymmetric");
                        String message_decrypted=this.symmetric.decryptt(received.get(3),symmetrickey);
                        String res=storeMessage(message_decrypted);
                        System.out.println(res);
                        oos.writeObject(this.symmetric.encryptt(res,symmetrickey));
                    }
                    else if(received.get(0).equals("sent_message_encrypted_Asymmetric_new_num"))
                    {
                        System.out.println("sent_message_encrypted_Asymmetric");
                        String message_decrypted=this.symmetric.decryptt(received.get(3),symmetrickey);
                        String res=storeMessage(message_decrypted);
                        System.out.println(res);
                        oos.writeObject(this.symmetric.encryptt(res ,symmetrickey));
                    }
                    else if (received.get(0).equals("show_encrypt_message_Asymmetric")) {
                       String res= getAllMessageForClient(received.get(1).toString());
                        ArrayList<String> responce = new ArrayList<String>();
                        //encrypt of message
                        String  message_encrypted = this.symmetric.encryptt(res, symmetrickey);
                        responce.add(message_encrypted);
                        //add mac
                        responce.add(java.util.Arrays.toString(symmetric.MAC(responce,symmetrickey)));
                        //return  response
                        oos.writeObject(responce);
                    }
                    else if (received.get(0).equals("get_publickey")) {

                        //Send Serve public Key
                        PublicKey publicKey = keypair.getPublic();
                        oos.writeObject(publicKey);

                        //Receive Session key
                        Scanner scan = new Scanner(clientSocket.getInputStream());
                        String strCSK=scan.nextLine();
                        byte[] decSK=DatatypeConverter.parseHexBinary(strCSK);

                        //decrypt session key
                        strCSK=Hyper.Decrept2(decSK,keypair.getPrivate());
                        decSK=DatatypeConverter.parseHexBinary(strCSK);
                        SecretKey secretKey= new SecretKeySpec(decSK, 0, decSK.length, "AES");
                        System.out.println("The Session key  is: " + DatatypeConverter.printHexBinary(secretKey.getEncoded()));
                        symmetrickey=secretKey;
                        //send acceptance message
                        oos.writeObject("The acceptance message received");
                    }
                    else if(received.get(0).equals("get_key"))
                    {
                        //receive client public key
                        Scanner in = new Scanner(clientSocket.getInputStream());
                        String CPK = in.nextLine();
                        byte[] buffer= DatatypeConverter.parseHexBinary(CPK);
                        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
                        clientPK = keyFactory.generatePublic(keySpec);
                        System.out.println("Public key received .." +
                                "\n client public key: "+DatatypeConverter.printHexBinary(clientPK.getEncoded()));
                        //send public key
                        PublicKey publicKey=keypair.getPublic();
                        System.out.println("sending server public key .." +
                                "\n "+DatatypeConverter.printHexBinary(publicKey.getEncoded()));
                        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                        out.println(DatatypeConverter.printHexBinary(publicKey.getEncoded()));
                    }
                    else if(received.get(0).equals("sent_message_encrypted_digital"))
                    {
                        // receive signing message
                        System.out.println("receiving request ..");
                        String request=received.get(3).toString();
                        //separate message, signing
                        String str[]=getResponseSeparateSign(request);
                        //decrypt message
                        String decryptedRequest=Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]),keypair.getPrivate());
                        System.out.println("decrypted message: "+decryptedRequest);
                        String response;
                        String res = "";
                        //verify signing
                        if (!DigitalSignature.Verify_Digital_Signature(
                                decryptedRequest.getBytes(),
                                DatatypeConverter.parseHexBinary(str[1]), clientPK)) {
                            System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
//                            // out.println(err);
                        }
                        else {
                            res = storeMessage(decryptedRequest);
                        }
                        //  sign response
                        String signResponse=getSignMessage(res,clientPK);
                        //send signing response
                        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                        out.println(signResponse);
                    }
                    else if(received.get(0).equals("sent_message_encrypted_digital_new_num"))
                    {
                        // receive signing message
                        System.out.println("receiving request ..");
                        String request=received.get(3).toString();
                        //separate message, signing
                        String str[]=getResponseSeparateSign(request);
                        //decrypt message
                        String decryptedRequest=Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]),keypair.getPrivate());
                        System.out.println("decrypted message: "+decryptedRequest);
                        String response;
                        String res = "";
                        //verify signing
                        if (!DigitalSignature.Verify_Digital_Signature(
                                decryptedRequest.getBytes(),
                                DatatypeConverter.parseHexBinary(str[1]), clientPK)) {
                            System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
//                            // out.println(err);
                        }
                        else {
                            res = storeMessage(decryptedRequest);
                        }
                        //  sign response
                        String signResponse=getSignMessage(res,clientPK);
                        //send signing response
                        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                        out.println(signResponse);
                    }
                    else if(received.get(0).equals("show_message_encrypted_digital"))
                    {
                        System.out.println("receiving request ..");
                        String request=received.get(1).toString();
                        System.out.println("request message: "+request);
                        //separate phone, signing
                        String str[]=getResponseSeparateSign(request);
                        //decrypt message
                        String phone=Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]),keypair.getPrivate());
                        System.out.println("decrypted message: "+phone);
                        String response;
                        String res = "";
//                        //verify signing
                        if (!DigitalSignature.Verify_Digital_Signature(
                                phone.getBytes(),
                                DatatypeConverter.parseHexBinary(str[1]), clientPK)) {
                            System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
                        }
                        else {
                            //get response
                            res = getAllMessageForClient(phone);
                        }
                        //  sign response
                        String signResponse=getSignMessage(res,clientPK);
                        //send signing response
                        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                        out.println(signResponse);
                    }
                    else if (received.get(0).equals("exchange_the_certificate"))
                    {
                        System.out.println("exchange_the_certificate "+certificate);
                        if (certificate==null)
                            getCertificate();
                        if (CAPublickey==null)
                            getCApublickey();

                        System.out.println("CAPublickey "+DatatypeConverter.printHexBinary(CAPublickey.getEncoded()));

                        //sent Certificate to Client
                        oos.writeObject(certificate);

                        //read Client Certificate
                      ClientCertificate=(Certificate) ois.readObject();

                        System.out.println("Client Certificate is  "+ ClientCertificate.Subject +" "+ClientCertificate.Signature.length+"\n"+DatatypeConverter.printHexBinary(ClientCertificate.publicKey.getEncoded()));

                        if(!DigitalSignature.Verify_Digital_Signature(ClientCertificate.Subject.getBytes(),ClientCertificate.Signature,CAPublickey)){
                            System.out.println("The signature is incorrect ... Retry sending");
                            break;
                        }
//                        if (!ClientCertificate.Subject.equals("0992039461")){
//                            System.out.println("The address is incorrect ... Retry sending");
//                            break;
//                        }


                    }  else if (received.get(0).equals("get_PubClient")){
                        System.out.println("get_PubClient");
                        oos.writeObject(connect.getpublicKey(received.get(1)));

                    }
                    else if (received.get(0).equals("sent_message_endToend")){
                        //read the socket response
                        Scanner in = new Scanner(clientSocket.getInputStream());
                        String signResponse = in.nextLine();
                        System.out.println("receive signed response: " + signResponse);
                        //get [response,signing]
                        String str[] = getResponseSeparateSign(signResponse);
                        //decrypt message
                        String data=Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]),keypair.getPrivate());
                        System.out.println("decrypted message: "+data);
                        String response;
                        String res = "";
//                        //verify signing
                        if (!DigitalSignature.Verify_Digital_Signature(
                                data.getBytes(),
                                DatatypeConverter.parseHexBinary(str[1]), ClientCertificate.publicKey)) {
                            System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
                        }
                        else {
                            //get response
                             res= connect.SendMessageEnc(received.get(1),received.get(2),received.get(3));
                        }
                        //  sign response
                        String signResponse1=getSignMessage(res,ClientCertificate.publicKey);
                        //send signing response
                        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                        out.println(signResponse1);
//                            String res= connect.SendMessageEnc(received.get(1),received.get(2),received.get(3));
//                            oos.writeObject(res);

                    }
                    else if (received.get(0).equals("sent_message_endToend_new_num")){
                        //read the socket response
                        Scanner in = new Scanner(clientSocket.getInputStream());
                        String signResponse = in.nextLine();
                        System.out.println("receive signed response: " + signResponse);
                        //get [response,signing]
                        String str[] = getResponseSeparateSign(signResponse);
                        //decrypt message
                        String data=Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]),keypair.getPrivate());
                        System.out.println("decrypted message: "+data);
                        String response;
                        String res = "";
//                        //verify signing
                        if (!DigitalSignature.Verify_Digital_Signature(
                                data.getBytes(),
                                DatatypeConverter.parseHexBinary(str[1]), ClientCertificate.publicKey)) {
                            System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
                        }
                        else {
                            //get response
                            res= connect.SendMessageEnc(received.get(1),received.get(2),received.get(3));
                        }
                        //  sign response
                        String signResponse1=getSignMessage(res,ClientCertificate.publicKey);
                        //send signing response
                        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                        out.println(signResponse1);
//                            String res= connect.SendMessageEnc(received.get(1),received.get(2),received.get(3));
//                            oos.writeObject(res);
                    }
                    else if (received.get(0).equals("show_message_EndToEnd")){
                        //read the socket response
                        Scanner in = new Scanner(clientSocket.getInputStream());
                        String signResponse = in.nextLine();
                        System.out.println("receive signed response: " + signResponse);
                        //get [response,signing]
                        String str[] = getResponseSeparateSign(signResponse);
                        //decrypt message
                        String data=Hyper.DecrepR(DatatypeConverter.parseHexBinary(str[0]),keypair.getPrivate());
                        System.out.println("decrypted message: "+data);
                        String response;
                        //String res = "";
                       // ArrayList<String> res = (ArrayList<String>) ois.readObject();
//                        //verify signing
                        if (!DigitalSignature.Verify_Digital_Signature(
                                data.getBytes(),
                                DatatypeConverter.parseHexBinary(str[1]), ClientCertificate.publicKey)) {
                            System.out.println("error .. data integrity error or unknown resource.. \n please try again later ..");
                        }
                        else {
                            //get response
//                          res= connect.getAllMessageEnc(received.get(1));
                            ArrayList<String>res= connect.getAllMessageEnc(received.get(1));
                        oos.writeObject(res);
                        }

                    }

                }

                    //terminate the server if client sends exit request
                 }catch(IOException ioException){
                    ioException.printStackTrace();
                } catch(ClassNotFoundException classNotFoundException){
                    classNotFoundException.printStackTrace();
                } catch (Exception e) {
                e.printStackTrace();
            }


        }
        private void getCApublickey() throws Exception {
            try (Socket socket = new Socket("127.0.0.1", 22222)) {
                ObjectOutputStream   oos = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream  ois = new ObjectInputStream(socket.getInputStream());

                //
                ArrayList ServerRequest = new ArrayList();
                CSR csr = new CSR(ServerSide.Subject, ServerSide.keypair.getPublic());
                ServerRequest.add(CAMultiThreading.PubKey);
                oos.writeObject(ServerRequest);

                //get CA publickey from CA
                CAPublickey= (PublicKey) ois.readObject();
            }

        }
        public void getCertificate() throws Exception {
            try (Socket socket = new Socket("127.0.0.1", 22222)) {
                ObjectOutputStream  oos=new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream ois=new ObjectInputStream(socket.getInputStream());

                ArrayList ServerRequest=new ArrayList();
                CSR csr=new CSR(ServerSide.Subject,ServerSide.keypair.getPublic());
                ServerRequest.add(CAMultiThreading.CREATE);
                ServerRequest.add(csr);
                oos.writeObject(ServerRequest);

                certificate = (Certificate)ois.readObject();

                System.out.println("Server Certificate is "+ certificate.Subject +" "+certificate.Signature.length);

            }

        }
        public String storeMessage( String message) throws Exception {
            String phone =  received.get(1).toString();
            String phone2 = received.get(2).toString();
            String res= connect.SendMessage(phone,phone2,message);
            return res;
        }

        public String getAllMessageForClient(String phone)
        {
            return connect.getAllMessage(phone);
        }

        public String show_message2(String decryptedRequest)
        {
            return connect.getAllMessage(decryptedRequest);
        }
        public static String getSignMessage(String message,PublicKey key)throws Exception{
            String result;

            //generate signature
            byte[] digitSign=DigitalSignature.Create_Digital_Signature(message.getBytes(),keypair.getPrivate());

            //encrypt request
            byte[] cypherRequest= Hyper.Encrept(message,key);

            // cypherRequest + signature
            Map<String, String> map = new HashMap<>();
            map.put("request", DatatypeConverter.printHexBinary(cypherRequest));
            map.put("signature", DatatypeConverter.printHexBinary(digitSign));

            Gson gson = new Gson();

            result = gson.toJson(map);

            return result;
        }
        private static String[] getResponseSeparateSign(String response) throws Exception {
            //parsing and extracting Request data
            Gson gson = new Gson();
            Map<String, String> req = gson.fromJson(response, Map.class);
            if(req.get("signature")!=null) {
                return new String[]{req.get("request"),req.get("signature")};
            } else {
                return null;
            }
        }
            }
        }

